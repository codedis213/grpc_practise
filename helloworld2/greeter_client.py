import grpc
import helloworld_pb2_grpc
import helloworld_pb2
import logging
from google.protobuf.any_pb2 import Any
# from google.protobuf.struct_pb2 import Struct


def run():
    channel = grpc.insecure_channel('localhost:50051')
    stub = helloworld_pb2_grpc.GreeterStub(channel)
    req_obj = helloworld_pb2.HelloRequest()
    req_obj.type = "SYSTEM"
    req_obj.slug = "jobs"

    req_obj.display_name.singular = "Job"
    req_obj.display_name.plural = "Jobs"

    req_obj.group = "Order Management"
    req_obj.version = 1
    req_obj.enabled = True
    req_obj.description = "Jobs module configurations"

    lst_obj = req_obj.config.views.list.add()

    lst_obj.name = "Default List"
    lst_obj.columns.add(ref_slug="job_title")

    field_group_obj = req_obj.config.field_groups.add(
        label="Duration & Description", slug="duration_and_description")
    field_group_obj.fields.add(
        source="SYSTEM",
        label="Estimated Job Start Date",
        slug="job_start_date",
        type="DATE",
        required=True
    )

    req_obj.config.workflows.onBeforeFormLoad.extend(["jai", "deep"])
    req_obj.config.workflows.onFormFieldValueChange.extend(["kaya", "maya"])

    perm_obj = req_obj.config.permissions.add()
    perm_obj.slug = "job_create"
    perm_obj.label = "Create new Job"
    perm_obj.roles.append("*")
    perm_obj.users.extend(["jai", "deep", "kaya", "maya"])

    response = stub.SayHello(req_obj)
    print("Greeter client received: " + response.message)


if __name__ == '__main__':
    logging.basicConfig()
    run()
